class ServiceHoursController < ApplicationController
  before_action :authorize_action,
    only: [:index, :show, :new, :create, :show_user, :admin, :approve_hours, :disapprove_hours, :hours_pdf]

  def new
   @service_hour = ServiceHour.new
  end

  def edit
    @service_hour = ServiceHour.find(params[:id])

    authorize @service_hour
  end

  def create
    @service_hour = ServiceHour.new(service_hour_params)
    @service_hour.day = DateTime.parse(service_hour_params[:day])

    if current_user
      @service_hour.recipient_id = current_user.id
    end

    if @service_hour.save
      redirect_to action: "index"
    else
      render 'new'
    end
  end

  def update
    @service_hour = ServiceHour.find(params[:id])

    authorize @service_hour

    if @service_hour.update(service_hour_params)
      redirect_to actions: "index"
    else
      render 'edit'
    end
  end

  def show
    @service_hour = ServiceHour.find(params[:id])
  end

  def index
    @service_hours = ServiceHour.where(recipient_id: current_user.id)
    @approved_hours = ServiceHour.where(recipient_id: current_user.id).where(service_hour_status: 2)

    @total_hours = 0

    @approved_hours&.each do |counting|
      @total_hours = @total_hours + counting.hours_and_money
    end

    p = ActiveRecord::Base.establish_connection
    c = p.connection
    @class_board = c.execute("SELECT p.label,
                                     CAST(SUM(DISTINCT s.hours+(COALESCE(s.money,0)/15)) AS FLOAT) as hours
                                FROM service_hours s
                                JOIN users u ON u.id = s.recipient_id
                                JOIN pledge_classes p ON p.id = u.pledge_class_id
                               WHERE s.service_hour_status_id = 2
                                 AND hours IS NOT NULL
                            GROUP BY p.label
                            ORDER BY hours DESC
                               LIMIT 5;")
    @brother_board = c.execute("SELECT (u.first_name || ' ' || u.last_name) as name,
                                       CAST(SUM(DISTINCT s.hours+(COALESCE(s.money,0)/15)) AS FLOAT) as hours
                                  FROM service_hours s
                                  JOIN users u ON u.id = s.recipient_id
                                 WHERE s.service_hour_status_id = 2
                                   AND hours IS NOT NULL
                              GROUP BY name
                              ORDER BY hours DESC
                                 LIMIT 5;")

  end

  def admin
    @users = User.where(affiliated_organization: current_user&.affiliated_organization)

    @not_started = 0
    @in_progress = 0
    @done = 0

    @users&.each do |user|
      if user.service_hours == 0
        @not_started = @not_started + 1
      elsif user.service_hours > 0 and user.service_hours < 10
        @in_progress = @in_progress + 1
      else
        @done = @done + 1
      end
    end

    @unapproved_hours = ServiceHour.find_by_sql("SELECT s.id,s.for,s.recipient_id,s.hours,s.money
                                                   FROM service_hours s
                                                   JOIN users u ON u.id = s.recipient_id
                                                  WHERE u.affiliated_organization = 1
                                                    AND s.service_hour_status_id = 1")
  end

  def show_user
    @user = User.find(params[:service_hour_id])
    @service_hours = ServiceHour.where(recipient_id: params[:service_hour_id])
    @approved_hours = ServiceHour.where(recipient_id: params[:service_hour_id]).where(service_hour_status: 2)

    @total_hours = 0

    @approved_hours&.each do |counting|
      @total_hours = @total_hours + counting.hours_and_money
    end
  end

  def destroy
    @service_hour = ServiceHour.find(params[:id])

    authorize @service_hour

    @service_hour.delete

    redirect_to service_hours_path
  end

  def approve_hours
    @hour = ServiceHour.find(params[:service_hour_id])
    @hour.update_attributes(:service_hour_status_id => 2)

    ServiceHourMailer.hour_approved_notification(@hour).deliver

    redirect_to :back
  end

  def disapprove_hours
    @hour = ServiceHour.find(params[:service_hour_id])
    @hour.update_attributes(:service_hour_status_id => 3)

    redirect_to :back
  end

  def hours_pdf
    @service_hours = ServiceHour.joins(:recipient).where('users.affiliated_organization': current_user&.affiliated_organization).where(service_hour_status: 2).order(recipient: :asc)
    @organization = Organization.find(current_user&.affiliated_organization)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Brother Hours This Semester",
        template: "pdfs/service_hours.html.erb",
        layout: "pdf.html",
        page_size: 'Letter'
      end
    end
  end

  private

  def service_hour_params
    params.require(:service_hour).permit(
      :title,
      :for,
      :description,
      :day,
      :hours,
      :money,
      :archived,
      :service_hour_status_id
    )
  end

  def authorize_action
    authorize ServiceHour
  end
end
