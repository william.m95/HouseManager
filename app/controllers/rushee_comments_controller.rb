class RusheeCommentsController < ApplicationController
  before_action :authorize_action,
    only: [:new, :edit, :update, :create]

  def new
    @rushee_comment = RusheeComment.new
  end

  def edit
    @rushee_comment = RusheeComment.find(params[:id])
  end

  def create
    @rushee_comment = RusheeComment.new(rushee_comment_params)

    authorize @rushee_comment

    if @rushee_comment.save
      redirect_to rushee_path(@rushee_comment.rushee)
    else
      render 'rushees/show'
    end
  end

  def update
    @rushee_comment = RusheeComment.find(params[:id])

    authorize @rushee_comment

    @rushee = @rushee_comment.rushee

    @rushee_comment.update(rushee_comment_params)

    redirect_to @rushee
  end

  def destroy
    @rushee_comment = RusheeComment.find(params[:id])

    authorize @rushee_comment

    @rushee = @rushee_comment.rushee

    @rushee_comment.destroy

    redirect_to @rushee
  end

  private

  def rushee_comment_params
    params.require(:rushee_comment).permit(
      :rushee_id,
      :author_id,
      :body
    )
  end

  def authorize_action
    authorize RusheeComment
  end
end
