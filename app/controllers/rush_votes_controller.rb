class RushVotesController < ApplicationController
  before_action :authorize_action,
    only: []

  def destroy
    @rush_vote = RushVote.find(params[:id])

    authorize @rush_vote

    @rush_vote.delete

    redirect_to rush_vote_path
  end

  private

  def rushee_params
    params.require(:rushee).permit(
      :rushee_id,
      :organization_id,
      :yes,
      :maybe,
      :no,
      :willing_contact
    )
  end

  def authorize_action
    authorize RushVote
  end
end
