class RusheesController < ApplicationController
  before_action :authorize_action,
    only: [:index, :admin, :show, :new, :create, :slideshow, :add_visit]
  after_action :authorize_action,
    only: [:vote_yes, :vote_no, :contact_volunteer]

  def new
   @rushee = Rushee.new
  end

  def edit
    @rushee = Rushee.find(params[:id])

    authorize @rushee
  end

  def create
    @rushee = Rushee.new(rushee_params)

    if current_user&.affiliated_organization
      @rushee.organization_id = current_user.affiliated_organization
    end

		if @rushee.save
      redirect_to action: "new"
    end
  end

  def update
    @rushee = Rushee.find(params[:id])

    authorize @rushee

    if @rushee.update(rushee_params)
      redirect_to actions: :index
    else
      render 'edit'
    end
  end

  def show
    @rushee          = Rushee.find(params[:id])
    @rushee_comments = RusheeComment.where(rushee_id: @rushee.id).order(created_at: :asc)
    @rushee_comment  = @rushee.rushee_comments.build(author: current_user)
    @contacts        = RushVote.where(rushee_id: @rushee.id).where(willing_contact: true).order(created_at: :asc)
    @rush_votes      = RushVote.where(voter_id: current_user&.id).where(rushee_id: @rushee.id)
  end

  def index
    @rushees = Rushee.where(organization_id: current_user&.affiliated_organization).order(name: :asc)
  end

  def admin
    @organization = Organization.find(current_user&.affiliated_organization)

    @rushee_list = Rushee.where(organization_id: current_user&.affiliated_organization).order(rushee_status_id: :asc).order(name: :asc)
    @vote_list = RushVote.where(voter_id: current_user&.id)

    @accepted_count = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 3).count
    @new_rushees = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 1).order(name: :asc)
    @outstanding_bids = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 2).order(name: :asc)
    @accepted_bids = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 3).order(name: :asc)
    @will_not_bid = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 4).order(name: :asc)
    @need_more_opinions = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 5).order(name: :asc)
    @will_bids = Rushee.where(organization_id: current_user&.affiliated_organization).where(rushee_status_id: 6).order(name: :asc)

    @votes_total = RushVote.where(organization_id: current_user&.affiliated_organization).count
    @votes_today = RushVote.where(organization_id: current_user&.affiliated_organization).where(created_at: Time.now.beginning_of_day..Time.now.end_of_day).count
    @comments_total = RusheeComment.joins(:author).where('users.affiliated_organization' => current_user&.affiliated_organization).count
    @comments_today = RusheeComment.joins(:author).where('users.affiliated_organization' => current_user&.affiliated_organization).where(created_at: Time.now.beginning_of_day..Time.now.end_of_day).count
    @rushees_total = Rushee.where(organization_id: current_user&.affiliated_organization).count
    @rushees_today = Rushee.where(organization_id: current_user&.affiliated_organization).where(here: true).count
  end

  def slideshow
    @rushee = Rushee.find(params[:rushee_id])
    @rushee_comments = RusheeComment.where(rushee_id: @rushee.id).order(created_at: :asc)
  end

  def destroy
    @rushee = Rushee.find(params[:id])

    authorize @rushee

    RushVote.where(rushee_id: @rushee.id).delete_all
    RusheeComment.where(rushee_id: @rushee.id).delete_all
    @rushee.delete

    redirect_to :back
  end

  def vote_yes
    @rushee    = Rushee.find(params[:rushee_id])
    @rush_vote = RushVote.where(rushee_id: @rushee).where(voter_id: current_user&.id)[0]

    if @rush_vote
      if @rush_vote.yes
        @rush_vote.update_attributes(:yes => false)
      else
        @rush_vote.update_attributes(:yes => true)
        @rush_vote.update_attributes(:no => false)
      end
    else
      RushVote.create(
        :rushee_id => @rushee[:id],
        :organization_id => current_user&.affiliated_organization,
        :voter_id => current_user&.id,
        :yes => true
      )
    end

    redirect_to :back
  end

  def vote_no
    @rushee = Rushee.find(params[:rushee_id])
    @rush_vote = RushVote.where(rushee_id: @rushee).where(voter_id: current_user&.id)[0]
    if @rush_vote
      if @rush_vote.no
        @rush_vote.update_attributes(:no => false)
      else
        @rush_vote.update_attributes(:no => true)
        @rush_vote.update_attributes(:yes => false)
      end
    else
      RushVote.create(
        :rushee_id => @rushee[:id],
        :organization_id => current_user&.affiliated_organization,
        :voter_id => current_user&.id,
        :no => true
      )
    end

    redirect_to :back
  end

  def contact_volunteer
    @rushee = Rushee.find(params[:rushee_id])
    @rush_vote = RushVote.where(rushee_id: @rushee).where(voter_id: current_user&.id)[0]
    if @rush_vote
      if @rush_vote.willing_contact
        @rush_vote.update_attributes(:willing_contact => false)
      else
        @rush_vote.update_attributes(:willing_contact => true)
      end
    else
      RushVote.create(
        :rushee_id => @rushee[:id],
        :organization_id => current_user&.affiliated_organization,
        :voter_id => current_user&.id,
        :willing_contact => true
      )
    end
    redirect_to :back
  end

  def add_visit
    @rushee = Rushee.find(params[:rushee_id])
    visits = @rushee.visits
    @rushee.update_attributes(:visits => visits + 1)
    @rushee.update_attributes(:here => true)
    RusheeMailer.contact_notification(@rushee).deliver
    redirect_to action: "new"
  end

  private

  def rushee_params
    params.require(:rushee).permit(
      :name,
      :email,
      :phone_number,
      :instagram,
      :facebook,
      :rushee_status_id,
      :contact_id,
      :photo,
      :bio
    )
  end

  def rushee_comment_params
    params.require(:rushee_comment).permit(
      :rushee_id,
      :author_id,
      :body
    )
  end

  def authorize_action
    authorize Rushee
  end
end
