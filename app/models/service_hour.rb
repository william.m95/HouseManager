# == Schema Information
#
# Table name: service_hours
#
#  id                     :integer          not null, primary key
#  title                  :string           not null
#  for                    :string           not null
#  description            :string           not null
#  day                    :date             not null
#  hours                  :decimal(, )
#  money                  :decimal(, )
#  recipient_id           :integer          not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  service_hour_status_id :integer          default(1)
#  archived               :boolean          default(FALSE)
#

class ServiceHour < ApplicationRecord
  # Associations
  belongs_to :recipient, class_name: User, foreign_key: 'recipient_id'
  has_one :service_hour_status, class_name: ServiceHourStatus, foreign_key: 'service_hour_status_id'

  # Validations
  validates :title, presence: true

  validates :description, presence: true

  validates :recipient_id, presence: true

  validates :for, presence: true

  # Model Functions
  def money_to_hours
    if money
      money / 15
    else
      0
    end
  end

  def hours_and_money
    if !hours
      self.hours = 0
    end

    self.hours + self.money_to_hours
  end

  def recipient_name
    User.find(self.recipient_id).full_name
  end

end
