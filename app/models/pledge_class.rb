# == Schema Information
#
# Table name: pledge_classes
#
#  id    :integer          not null, primary key
#  label :string
#

class PledgeClass < ApplicationRecord
  belongs_to :user, optional: true
end
