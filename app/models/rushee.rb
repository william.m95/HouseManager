# == Schema Information
#
# Table name: rushees
#
#  id               :integer          not null, primary key
#  name             :string           not null
#  email            :string           not null
#  photo            :string           default("https://upload.wikimedia.org/wikipedia/commons/9/93/Default_profile_picture_%28male%29_on_Facebook.jpg")
#  phone_number     :string           not null
#  description      :string
#  instagram        :string
#  facebook         :string
#  bio              :string
#  visits           :integer          default(1)
#  organization_id  :integer          not null
#  contact_id       :integer
#  rushee_status_id :integer          default(1)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  here             :boolean          default(TRUE)
#

class Rushee < ApplicationRecord
  # Associations
  belongs_to :organization, foreign_key: 'organization_id'
  has_one :user, foreign_key: 'contact_id'
  has_one :rushee_status, foreign_key: 'rushee_status_id'
  has_many :rushee_comments, dependent: :destroy

  # Validations

  # Methods
  def contact_name
    if self.contact_id
      User.find(self.contact_id).full_name
    else
      "Needs a contact"
    end
  end

  def status_label
    RusheeStatus.find(self.rushee_status_id).label
  end

  def default_photo
    "https://d2wnxi2v4fzw0h.cloudfront.net/assets/fallback/preview_default_profile.png"
  end

  def vote_count_yes
    RushVote.where(rushee_id: self.id).where(yes: true).count
  end

  def vote_count_no
    RushVote.where(rushee_id: self.id).where(no: true).count
  end

  def next_rushee
    self.class.where("id > ?", id).first
  end

  def self.reset_here_counters
    Rushee.all.each do |rushee|
      rushee.update_attributes(:here => false)
    end
  end
end
