# == Schema Information
#
# Table name: rush_votes
#
#  id              :integer          not null, primary key
#  rushee_id       :integer
#  organization_id :integer
#  voter_id        :integer
#  yes             :boolean          default(FALSE)
#  no              :boolean          default(FALSE)
#  willing_contact :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class RushVote < ApplicationRecord
  # Methods
  def volunteer_name
    if self.voter_id
      User.find(self.voter_id).full_name
    else
      "Needs a contact"
    end
  end

  def volunteer_photo
    if self.voter_id
      User.find(self.voter_id).image
    else
      "Needs a contact"
    end
  end
end
