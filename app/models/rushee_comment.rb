# == Schema Information
#
# Table name: rushee_comments
#
#  id         :integer          not null, primary key
#  rushee_id  :integer
#  author_id  :integer
#  body       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RusheeComment < ApplicationRecord
  # Associations
  belongs_to :rushee, foreign_key: 'rushee_id'
  belongs_to :author, class_name: User, foreign_key: 'author_id'

  # Validations
  validates :rushee_id, presence: true

  validates :author_id, presence: true

  validates :body, presence: true

  def author_image
    User.find(self.author_id).image
  end

  def author_name
    User.find(self.author_id).full_name
  end

  def post_date
    created_at.strftime("%m/%d/%Y")
  end
end
