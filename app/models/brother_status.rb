# == Schema Information
#
# Table name: brother_statuses
#
#  id    :integer          not null, primary key
#  label :string
#

class BrotherStatus < ApplicationRecord
  belongs_to :user, optional: true

  def string_label
    "#{label}"
  end
end
