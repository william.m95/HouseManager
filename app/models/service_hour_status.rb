# == Schema Information
#
# Table name: service_hour_statuses
#
#  id    :integer          not null, primary key
#  label :string
#

class ServiceHourStatus < ApplicationRecord
  belongs_to :service_hour, optional: true
end
