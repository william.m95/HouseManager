# == Schema Information
#
# Table name: rushee_statuses
#
#  id    :integer          not null, primary key
#  label :string
#

class RusheeStatus < ApplicationRecord
  belongs_to :rushee, optional: true

  # Model Functions
  def status_label
    "#{label}"
  end
end
