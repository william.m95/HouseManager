class NotificationMailer < ApplicationMailer
  def welcome_email(user_id)
    @user = User.find(user_id)

    mail(
      to: @user.email_address,
      subject: "Welcome to House Manager #{@user.full_name}!"
    )
  end

  def new_announcement_email(recipients, announcement_id)
    @announcement = Announcement.find(announcement_id)
    @author = User.find(@announcement.author_id)

    recipients.each do |recipient|
      mail to: "#{recipient}",
      subject: "New Annoucement by #{@author.full_name} - #{@announcement.title}"
    end
  end
end
