class ApplicationMailer < ActionMailer::Base
  default from: 'will@housemananger.me'

  add_template_helper(ApplicationHelper)
end
