class MaintenanceRequestMailer < ApplicationMailer
  def new_maintenance_request_notification(request)
    @request = request
    @users = User.joins(:roles).where(affiliated_organization: @request.organization_id).where(roles:{label: ['House Manager', 'Organization Manager']})
    emails = @users.collect(&:email_address).join(",")

    emails = emails + ",#{@request.reporter.email_address}"

    mail(:bcc => "#{emails}",
     :subject => "New Maintenance Request: #{@request.issue_title}")
  end

  def maintenance_request_started_notification(request)
    @request = request
    @users = User.joins(:roles).where(affiliated_organization: @request.organization_id).where(roles:{label: ['House Manager', 'Organization Manager']})

    emails = @users.collect(&:email_address).join(",")

    emails = emails + ",#{@request.reporter.email_address}"

    mail(:bcc => "#{emails}",
     :subject => "Maintenance Request Started: #{@request.issue_title}")
  end

  def maintenance_request_finished_notification(request)
    @request = request
    @users = User.joins(:roles).where(affiliated_organization: @request.organization_id).where(roles:{label: ['House Manager', 'Organization Manager']})

    emails = @users.collect(&:email_address).join(",")

    emails = emails + ",#{@request.reporter.email_address}"

    mail(:bcc => "#{emails}",
     :subject => "Maintenance Request Finished: #{@request.issue_title}")
  end
end
