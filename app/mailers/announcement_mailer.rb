class AnnouncementMailer < ApplicationMailer
  def new_announcement_notification(announcement)
    @announcement = announcement

    @users = User.where(affiliated_organization: @announcement.organization_id).where(enabled: true)
    emails = @users.collect(&:email_address).join(",")

    mail(:bcc => "#{emails}",
         :subject => "New Announcement by #{announcement.author_name} - #{announcement.title}")
  end
end
