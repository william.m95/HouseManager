class RusheeMailer < ApplicationMailer
  def contact_notification(rushee)
    @rushee = rushee

    @users = User.where(id: @rushee.contact_id)
    emails = @users.collect(&:email_address).join(",")

    mail(:bcc => "#{emails}",
         :subject => "Your contact #{rushee.name} has arrived @ #{Time.now.in_time_zone('America/New_York').strftime("%l:%M")}")
  end
end
