class ServiceHourMailer < ApplicationMailer
  def hour_approved_notification(record)
    @record = record
    @user   = @record.recipient

    @approved_hours = ServiceHour.where(recipient_id: @user.id).where(service_hour_status: 2)
    @total_hours = 0
    @approved_hours&.each do |counting|
      @total_hours = @total_hours + counting.hours_and_money
    end

    mail to: "#{@user.email_address}", subject: "HouseManager - Service Hour Approved: #{@record.title}"
  end

  def approval_list_notification
    @organizations = Organization.pluck(:id)
    @organizations&.each do |organization|
      @unapproved_hours = ServiceHour.find_by_sql ["SELECT s.id,s.for,s.recipient_id,s.hours,s.money
                                                      FROM service_hours s
                                                      JOIN users u ON u.id = s.recipient_id
                                                     WHERE u.affiliated_organization = ?
                                                       AND s.service_hour_status_id = 1", organization]
      if @unapproved_hours
        @user = User.joins(:roles).where(affiliated_organization: organization).where(roles:{label: ['Service Manager']})

        mail(to: "#{@user.email_address}",
        subject: "HouseManager - Service Hours to Approve").deliver
      end
    end
  end
end
