class RusheePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def new?
    show?
  end

  def create?
    show?
  end

  def index?
    show?
  end

  def show?
    user.has_role?(Role.where(label: ['Member']))
  end

  def destroy?
    user.has_role?(Role.where(label: ['System Admin', 'Rush Manager', 'Organization Manager'])) or user == record
  end

  def update?
    destroy?
  end

  def edit?
    destroy?
  end

  def admin?
    show?
  end

  def show_user?
    destroy?
  end

  def vote_yes?
    show?
  end

  def vote_no?
    vote_yes?
  end

  def add_visit?
    vote_yes?
  end

  def contact_volunteer?
    vote_yes?
  end

  def slideshow?
    show?
  end
end
