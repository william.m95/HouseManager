class RusheeCommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def new?
    show?
  end

  def create?
    show?
  end

  def index?
    show?
  end

  def show?
    user.has_role?(Role.where(label: ['Member']))
  end

  def destroy?
    show?
  end

  def update?
    destroy?
  end

  def edit?
    destroy?
  end
end
