# Preview all emails at http://localhost:3000/rails/mailers/service_hour_mailer
class ServiceHourMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/service_hour_mailer/hour_approved_notification
  def hour_approved_notification
    ServiceHourMailer.hour_approved_notification
  end

end
