# Preview all emails at http://localhost:3000/rails/mailers/maintenance_request_mailer
class MaintenanceRequestMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/maintenance_request_mailer/new_maintenance_request_notification
  def new_maintenance_request_notification
    MaintenanceRequestMailer.new_maintenance_request_notification
  end

end
