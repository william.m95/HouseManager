# Preview all emails at http://localhost:3000/rails/mailers/announcement_mailer
class AnnouncementMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/announcement_mailer/new_announcement_notification
  def new_announcement_notification
    AnnouncementMailer.new_announcement_notification
  end

end
