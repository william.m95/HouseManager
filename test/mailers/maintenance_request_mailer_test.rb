require 'test_helper'

class MaintenanceRequestMailerTest < ActionMailer::TestCase
  test "new_maintenance_request_notification" do
    mail = MaintenanceRequestMailer.new_maintenance_request_notification
    assert_equal "New maintenance request notification", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
