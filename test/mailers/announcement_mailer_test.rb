require 'test_helper'

class AnnouncementMailerTest < ActionMailer::TestCase
  test "new_announcement_notification" do
    mail = AnnouncementMailer.new_announcement_notification
    assert_equal "New announcement notification", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
