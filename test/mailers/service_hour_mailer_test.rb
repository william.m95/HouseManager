require 'test_helper'

class ServiceHourMailerTest < ActionMailer::TestCase
  test "hour_approved_notification" do
    mail = ServiceHourMailer.hour_approved_notification
    assert_equal "Hour approved notification", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
