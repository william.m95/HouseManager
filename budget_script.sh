ps aux | egrep -i 'puma' | awk '{print $2}' | xargs kill
ps aux | egrep -i 'unicorn' | awk '{print $2}' | xargs kill
bundle exec rake assets:precompile RAILS_ENV=production
unicorn -c config/unicorn.rb -E production -D
rails s -e production -p 3000 -b 107.170.25.73 -d
