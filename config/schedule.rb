# Every day resets the here to be false
every 1.day, at: '12:30 am' do
  runner 'Rushee.reset_here_counters'
end

# Doesn't work rn
every 1.day, at: '7:00 pm' do
  runner 'ServiceHourMailer.approval_list_notification.deliver'
end

# Removes old late plate requests
every '*/15 * * * *' do
  runner 'LatePlate.remove_old_plates'
end
