working_directory "/home/will/HouseManager"

# Set unicorn options
worker_processes 2
preload_app true
timeout 30

# Path for the Unicorn socket
listen "/home/will/HouseManager/shared/sockets/unicorn.sock", :backlog => 64

# Set path for logging
stderr_path "/home/will/HouseManager/shared/log/unicorn.stderr.log"
stdout_path "/home/will/HouseManager/shared/log/unicorn.stdout.log"

# Set proccess id path
pid "/home/will/HouseManager/shared/pids/unicorn.pid"

