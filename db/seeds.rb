# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require File.expand_path('../seeds/roles', __FILE__)
require File.expand_path('../seeds/rushee_statuses', __FILE__)
require File.expand_path('../seeds/maintenance_request_priorities', __FILE__)
require File.expand_path('../seeds/brother_statuses', __FILE__)
require File.expand_path('../seeds/pledge_classes', __FILE__)
