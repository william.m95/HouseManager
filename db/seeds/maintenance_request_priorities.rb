module MaintenanceRequestPriorities
  maintenance_request_priorities = [
    {
      label: "Low",
      description: "Fixed by end of month"
    },
    {
      label: "Medium",
      description: "Fixed by end of week"
    },
    {
      label: "High",
      description: "Fixed by end of day"
    },
    {
      label: "Immediate",
      description: "Fixing this as soon as possible"
    }
  ]

  maintenance_request_priorities.each do |priority|
    MaintenanceRequestPriority.find_or_create_by!(priority)
  end
end
