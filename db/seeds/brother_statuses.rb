module BrotherStatuses
  statuses = [
    {
      label: "Pledge"
    },
    {
      label: "Active"
    },
    {
      label: "Inactive/Nonactive"
    },
    {
      label: "Graduated"
    }
  ]

  statuses.each do |status|
    BrotherStatus.find_or_create_by!(status)
  end
end
