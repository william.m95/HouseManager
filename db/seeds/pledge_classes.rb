module PledgeClasses
  pledge_classes = [
    {
      label: "Fall '13"
    },
    {
      label: "Spring '14"
    },
    {
      label: "Fall '14"
    },
    {
      label: "Spring '15"
    },
    {
      label: "Fall '15"
    },
    {
      label: "Spring '16"
    },
    {
      label: "Fall '16"
    },
    {
      label: "Spring '17"
    },
    {
      label: "Fall '17"
    },
    {
      label: "Sprint '18"
    }
  ]

  pledge_classes.each do |classes|
    PledgeClass.find_or_create_by!(classes)
  end
end
