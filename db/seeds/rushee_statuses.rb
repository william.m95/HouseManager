module RusheeStatuses
  rushee_statuses = [
    {
      label: "New Rushee"
    },
    {
      label: "Bidded"
    },
    {
      label: "Accepted"
    },
    {
      label: "Will Not Bid"
    },
    {
      label: "Needs More Opinions"
    },
    {
      label: "Will Bid"
    }
  ]

  rushee_statuses.each do |status|
    RusheeStatus.find_or_create_by!(status)
  end
end
