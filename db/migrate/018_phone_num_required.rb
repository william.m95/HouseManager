class PhoneNumRequired < ActiveRecord::Migration[5.0]
  def change
    # Changes the rushee  phone number to be required
    Rushee.where(phone_number: nil).update_all(phone_number: 'N/A')
    change_column :rushees, :phone_number, :string, null: false
  end
end
