class CreateRush < ActiveRecord::Migration[5.0]
  def change
    create_table :rushee_statuses do |t|
      t.string :label
    end

    create_table :rushees do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :photo
      t.string :phone_number
      t.string :description
      t.string :instagram
      t.string :facebook
      t.string :bio
      t.integer :visits, default: 1
      t.integer :organization_id, null: false
      t.integer :contact_id
      t.integer :rushee_status_id, default: 1

      t.timestamps
    end

    add_foreign_key :rushees, :organizations, column:"organization_id", name: 'fk_rushee_organization'
    add_foreign_key :rushees, :users, column:"contact_id", name: 'fk_rushee_contact'
    add_foreign_key :rushees, :rushee_statuses, column:"rushee_status_id", name: 'fk_rushee_status'

    create_table :rushee_visit do |t|
      t.string :description
      t.date :day
    end

    change_table :organizations do |t|
      t.boolean :in_rush, default: false
      t.integer :rush_goal
      t.integer :rush_manager
    end

    add_foreign_key :organizations, :users, column:"rush_manager", name: 'fk_rush_manager'

    create_table :rush_votes do |t|
      t.integer :rushee_id
      t.integer :organization_id
      t.integer :voter_id
      t.boolean :yes, default: false
      t.boolean :maybe, default: false
      t.boolean :no, default: false
      t.boolean :willing_contact, default: false

      t.timestamps
    end

    add_foreign_key :rush_votes, :rushees, column:"rushee_id", name: 'fk_rush_votes_rushee'
    add_foreign_key :rush_votes, :users, column:"voter_id", name: 'fk_rush_votes_voter'
    add_foreign_key :rush_votes, :organizations, column:"organization_id", name: 'fk_rush_votes_organization'

    create_table :rushee_comments do |t|
      t.integer :rushee_id
      t.integer :author_id
      t.string :body, null: false

      t.timestamps
    end

    add_foreign_key :rushee_comments, :rushees, column:"rushee_id", name: 'fk_rushee_rushee_comments'
    add_foreign_key :rushee_comments, :users, column:"author_id", name: 'fk_rushee_comments'
  end
end
