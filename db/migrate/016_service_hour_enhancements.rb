class ServiceHourEnhancements < ActiveRecord::Migration[5.0]
  def change
    # Adds in brother statuses for people like graduated brothers to not see stuff
    create_table :brother_statuses do |t|
      t.string :label
    end

    # Used for leaderboards
    create_table :pledge_classes do |t|
      t.string :label
    end

    change_table :users do |t|
      t.integer :current_status_id
      t.integer :pledge_class_id
    end

    add_foreign_key :users, :brother_statuses, column:"current_status_id", name:'fk_brother_status'
    add_foreign_key :users, :pledge_classes, column:"pledge_class_id", name:'fk_pledge_class'

    # Adds in a status set for service hours so they can be approved/disapproved
    # Also adds archival
    create_table :service_hour_statuses do |t|
      t.string :label
    end

    ServiceHourStatus.find_or_create_by!({label:"Pending"})
    ServiceHourStatus.find_or_create_by!({label:"Approved"})
    ServiceHourStatus.find_or_create_by!({label:"Disapproved"})

    change_table :service_hours do |t|
      t.integer :service_hour_status_id, default: 1
      t.boolean :archived, default: false
    end

    add_foreign_key :service_hours, :service_hour_statuses, column:"service_hour_status_id", name:'fk_service_hour_status'
  end
end
