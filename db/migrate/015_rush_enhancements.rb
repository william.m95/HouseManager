class RushEnhancements < ActiveRecord::Migration[5.0]
  def up
    # Adds a here column for when rushees check in at the table
    change_table :rushees do |t|
      t.boolean :here, default: true
    end

    # Changes the rushee stuff to default to the default photo instead of being blank on submit
    change_column :rushees, :photo, :string, :default => 'https://d2wnxi2v4fzw0h.cloudfront.net/assets/fallback/preview_default_profile.png'

    # Removes the unused maybe vote type from rush votes
    remove_column :rush_votes, :maybe, :boolean
  end

  def down
    add_column :rush_votes, :maybe, :boolean
    change_column :rushees, :photo, :string
    remove_column :rushees, :here, :boolean
  end
end
