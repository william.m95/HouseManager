class PropPicChange < ActiveRecord::Migration[5.0]
  def up
    # Changes the rushee stuff to default to the default photo instead of being blank on submit
    change_column :rushees, :photo, :string, :default => 'https://upload.wikimedia.org/wikipedia/commons/9/93/Default_profile_picture_%28male%29_on_Facebook.jpg'
  end

  def down
    change_column :rushees, :photo, :string
  end
end
